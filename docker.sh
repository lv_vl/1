#!/bin/bash

# Download and install Docker
wget -qO- get.docker.com | bash

# Check Docker version
docker -v

# Enable Docker service
systemctl enable docker

# Download and install Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/v2.18.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Check Docker Compose version
docker-compose --version
