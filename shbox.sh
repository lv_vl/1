#!/bin/bash

red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
end='\033[0m'

function agent {
    snap remove oracle-cloud-agent
}


function ubuntu_port {
    iptables -P INPUT ACCEPT
    iptables -P FORWARD ACCEPT
    iptables -P OUTPUT ACCEPT
    iptables -F
    apt-get purge netfilter-persistent #规则持久化工具
}

function bbr {
    wget -N --no-check-certificate "https://raw.githubusercontent.com/chiakge/Linux-NetSpeed/master/tcp.sh" && chmod +x tcp.sh && ./tcp.sh
}

function root_login {
    read -p "请输入新的密码：" new_password
    echo -e "$new_password\n$new_password" | sudo passwd root
    #echo "$new_password" | sudo passwd root
    sudo sed -i 's/^#\?PermitRootLogin.*/PermitRootLogin yes/g' /etc/ssh/sshd_config
    sudo sed -i 's/^#\?PasswordAuthentication.*/PasswordAuthentication yes/g' /etc/ssh/sshd_config
    sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config.d/60-cloudimg-settings.conf
    sudo service ssh restart
    echo -e "${green}密码修改成功！${end}"
}


function media_v4 {
    bash <(curl -L -s https://gitlab.com/lv_vl/1/-/raw/1/media.sh) -M 4
}

function media_v6 {
    bash <(curl -L -s https://gitlab.com/lv_vl/1/-/raw/1/media.sh) -M 6
}

function 3xui {
    bash <(curl -Ls https://gitlab.com/lv_vl/1/-/raw/1/3install.sh)
}

function xui {
    bash <(curl -Ls https://gitlab.com/lv_vl/1/-/raw/1/install.sh)
}

function fxui {
    bash <(curl -Ls https://gitlab.com/lv_vl/1/-/raw/1/finstall.sh)
}


function besttrace {
    echo -e "按任意键安装,${red}按u删除并退出${end}"
    local choice
    read choice

    if [ "$choice" == "u" ]; then
        rm -rf /usr/local/bin/btrace
        echo -e "${green}besttrace已删除!${end}"
    else
        arch=$(uname -m)

        if [ "$arch" == "x86_64" ]; then
            arch="amd64"
        elif [ "$arch" == "aarch64" ]; then
            arch="arm64"
        else
            echo "不支持的系统架构：$arch"
            exit 1
        fi

        wget --no-check-certificate -O /usr/local/bin/btrace https://gitlab.com/lv_vl/1/-/raw/1/besttrace-$arch
        chmod +x /usr/local/bin/btrace
        alias bt='/usr/local/bin/btrace -g cn -T'
        #btrace -g cn -T root.tgtg.eu.org

    fi
}

function tcping {
    echo -e "按任意键安装,${red}按u删除并退出${end}"
    local choice
    read choice

    if [ "$choice" == "u" ]; then
        rm -rf /usr/local/bin/tcp
        echo -e "${green}tcping已删除!${end}"
    else
        arch=$(uname -m)

        if [ "$arch" == "x86_64" ]; then
            arch="amd64"
        elif [ "$arch" == "aarch64" ]; then
            arch="arm64"
        else
            echo "不支持的系统架构：$arch"
            exit 1
        fi

        wget --no-check-certificate -O /usr/local/bin/tcp https://gitlab.com/lv_vl/1/-/raw/1/tcp-$arch
        chmod +x /usr/local/bin/tcp
        tcp ddns.tgtg.eu.org:88

    fi
}

function docker {
    wget -qO- get.docker.com | bash
    docker -v
    systemctl enable docker
    echo -e "${green}docker安装成功!${end}"
}

function docker_compose {
    echo -e "${green}回车继续安装，${red}输入u卸载${end}"
    local choice
    read choice
    if [ "$choice" == "u" ]; then
        sudo rm -rf /usr/local/bin/docker-compose
        docker-compose -v
        echo -e "${green}docker-compos卸载成功!${end}"
    else
        sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
        sudo chmod +x /usr/local/bin/docker-compose
        docker-compose -v
        echo -e "${green}docker-compose安装成功!${end}"
    fi
    
}


function docker_compose_openwrt {
    echo -e "${green}回车继续安装，${red}输入u卸载${end}"
    local choice
    read choice
    if [ "$choice" == "u" ]; then
        rm -rf /bin/docker-compose
        docker-compose -v
        echo -e "${green}docker-compos卸载成功!${end}"
    else
        curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /bin/docker-compose
        chmod +x /bin/docker-compose
        docker-compose -v
        echo -e "${green}docker-compose安装成功!${end}"
    fi
    
}

function show_menu {
    echo -e ${green}"====================================="
    echo -e "1. 流媒体检测IPV4"
    echo -e "2. 流媒体检测IPV6"
    echo -e "3. oracle移除agent"
    echo -e "4. oracle开端口"
    echo -e "5. 3x-ui"
    echo -e "6. x-ui"
    echo -e "7. Fx-ui"
    echo -e "8. Besttrace安装/卸载"
    echo -e "9. Docker"
    echo -e "10. Docker-compose安装/卸载"
    echo -e "11. Docker-compose(openwrt)安装/卸载"
    echo -e "12. 设置root密码，改默认登录"
    echo -e "13. Tcping安装/卸载"
    echo -e "14. bbr"
    echo -e "0. 退出脚本"
    echo -e "====================================="${end}
}


while true; do
    show_menu

    # 读取用户输入
    read select

    # 使用case语句根据用户选择执行不同的命令
    case $select in
    1)
        media_v4
        exit 0
        ;;
    2)
        media_v6
        exit 0
        ;;
    3)
        agent
        ;;
    4)
        ubuntu_port
        ;;
    5)
        3xui
        exit 0
        ;;
    6)
        xui
        exit 0
        ;;
    7)
        fxui
        exit 0
        ;;
    8)
        besttrace
        exit 0
        ;;
    9)
        docker
        exit 0
        ;;
    10)
        docker_compose
        exit 0
        ;;
    11)
        docker_compose_openwrt
        exit 0
        ;;
    12)
        root_login
        exit 0
        ;;
    13)
        tcping
        exit 0
        ;;
    14)
        bbr
        exit 0
        ;;        
    0)
        exit 0
        ;;
    *)
        echo "无效的选项，请重新输入。"
        ;;
    esac
done

