#!/bin/bash
red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
end='\033[0m'

echo -e "按任意键安装,${red}按u删除并退出${end}"
read choice

if [ "$choice" == "u" ]; then
    rm -rf /usr/local/bin/btrace
    echo -e "${green}besttrace已删除!${end}"
else
    arch=$(uname -m)

    if [ "$arch" == "x86_64" ]; then
      arch="amd64"
    elif [ "$arch" == "aarch64" ]; then
      arch="arm64"
    else
      echo "不支持的系统架构：$arch"
      exit 1
    fi

    wget --no-check-certificate -O /usr/local/bin/btrace https://gitlab.com/lv_vl/1/-/raw/1/besttrace-$arch
    chmod +x /usr/local/bin/btrace
    btrace -g cn -T root.tgtg.eu.org

fi
