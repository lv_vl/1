#!/bin/bash
snap remove oracle-cloud-agent
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
iptables -F
apt-get purge netfilter-persistent #规则持久化工具
